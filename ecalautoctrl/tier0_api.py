'''
This module provide an general interface to the T0 API.
'''

import requests
from typing import Dict

class T0GeneralAPI():
    """
    This class provides access to the 'standard' T0 API which is accessible
    via HTTPS queries.
    """

    def __init__(self):
        self.t0_url = f'https://cmsweb.cern.ch/t0wmadatasvc/prod/'

    def __str__(self):
        return 'T0GeneralAPI'

    def get_MRH_values(self, run: int) -> Dict[str,str]:
        """
        Returns a dictionary of values of the express processing for the specified run.

        :param run:  CMS run number.
        """
        # Return object
        MRH_values = {}

        # Compose URL
        target_url = self.t0_url + 'express_config?run=' + str(run)

        # Query
        resp = requests.get(url=target_url, verify=False).json()['result']

        # Parse
        if len(resp):
            for dset in resp:
                MRH_values['cmssw']      = dset['cmssw']
                MRH_values['scram_arch'] = dset['scram_arch']
                MRH_values['global_tag'] = dset['global_tag']
        else:
            # if not available for the current run get the value
            # from the current run under processing in T0
            resp = requests.get(url=self.t0_url + '/express_config', verify=False).json()['result']
            if len(resp):
                for dset in resp:
                    MRH_values['cmssw']      = dset['cmssw']
                    MRH_values['scram_arch'] = dset['scram_arch']
                    MRH_values['global_tag'] = dset['global_tag']

        return MRH_values

    def get_prompt_gt(self, run: int) -> str:
        """
        Returns the Prompt GT as string for the specified run.

        :param run:  CMS run number.
        """
        # Return value
        t0gt = ''

        # Compose URL
        target_url = self.t0_url + 'reco_config?run=' + str(run)

        # Query
        resp = requests.get(url=target_url, verify=False).json()['result']

        # Parse
        if len(resp):
            for dset in resp:
                if 'ppEra' in dset['scenario']:
                    t0gt = dset['global_tag']
        else:
            # if global_tag not available for the current run get the value
            # from the current run under processing in T0
            resp = requests.get(url=self.t0_url + '/reco_config', verify=False).json()['result']
            if len(resp):
                for dset in resp:
                    if 'ppEra' in dset['scenario']:
                        t0gt = dset['global_tag']

        return t0gt
