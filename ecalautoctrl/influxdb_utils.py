from typing import Dict

def reformat_last(data : Dict) -> Dict:
    """
    Remove the automatically prepended 'last_' to results from queries involving last()

    :param data: data returned by InfluxDBClient.query
    """
    
    # copy keys to avoid 'dictionary changed during loop' error
    rdata = []  
    if len(data):
        for d in data.get_points():
            keys = list(d.keys())
            for key in keys:
                d[key.replace('last_', '')] = d.pop(key)
            rdata.append(d)
                
    return rdata
